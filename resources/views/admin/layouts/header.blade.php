<header>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="margin: 5px; ">
        <div>
            @php
                $fds=\Illuminate\Support\Facades\Auth::guard('user')->user()->id

            @endphp
            <a class="btn btn-outline-primary" href='{{url('userInfo/'.$fds)}}'
               role="button">
                {{\Illuminate\Support\Facades\Auth::guard('user')->user()->name}}
            </a>
        </div>
        <div>
            @if (Request::path() !== 'administrators')
                <a class="btn btn-outline-primary" href='{{url('/administrators')}}'
                   role="button">
                    Администраторы
                </a>
            @endif
        </div>
        <div style="margin-left: 90%">
            <a class="navbar-brand border"
               href="{{url('/logout')}}">
                Выйти
            </a>
        </div>
    </nav>

</header>

