@extends('admin.layouts.main')

@section('css')
@endsection

@section('js')
    {{--    <script src="/resources/js/user/user.js"></script>--}}
@endsection

@section('content')

    <div style="margin: 5px">
        <button type="button" class="btn btn-primary" onclick="window.location.href='/createUser'">
            Добавить пользователя
        </button>
    </div>

    <div class="box-body" style="margin: 5px">
        <div class="content-wrapper">
            <table class="table table-bordered">
                <thead>
                <th>Имя</th>
                <th>Email</th>
                <th>Telegram</th>
                <th>Status</th>
                <th></th>
                <th></th>
                </thead>

                <tbody>
                @foreach(@$administrators as $administrator)
                    <tr>
                        <td>{{$administrator->name}}</td>
                        <td>{{$administrator->email}}</td>
                        <td>{{$administrator->telegram}}</td>
                        <td>{{$administrator->status}}</td>
                        <td>
                            <form action="{{url('/editUser/'.$administrator->id)}}" method="get">
                                <button type="submit" class="btn btn-primary">
                                    Редактировать
                                </button>
                            </form>

                        </td>
                        <td>
                            <form action="{{url('/deleteUser/'.$administrator->id)}}" method="post">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger active">
                                    Удалить
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>

            </table>
        </div>
    </div>

@endsection
