@extends('admin.layouts.main')

@section('css')
@endsection

@section('content')
    @if (@$user)
        <form action="{{url('/updateUser/'.$user->id)}}" method="post" class="form-horizontal">
            @csrf
            <div class=form>
                <div class="form-group col-md-5">
                    <label for="name"> Имя </label>
                    <input type="text" class="form-control" id="name" name="name" value={{$user->name}}>
                </div>
                <div class="form-group col-md-5">
                    <label for="password"> Пароль </label>
                    <input type="text" class="form-control" id="password" name="password">
                </div>
                <div class="form-group col-md-5">
                    <label for="email"> Email </label>
                    <input type="text" class="form-control" id="email" name="email" value={{$user->email}}>
                </div>
                <div class="form-group col-md-5">
                    <label for="telegram"> Telegram </label>
                    <input type="text" class="form-control" id="telegram" name="telegram" value={{$user->telegram}}>
                </div>
                <div class="form-group col-md-5">
                    <label for="status"> Статус </label>
                    <select name="status" class="form-control" id="status">
                        <option>ADMIN</option>
                        <option>USER</option>
                        <option>GUEST</option>
                    </select>
                </div>
                <div class="btn">
                    <button type="submit" class="btn btn-primary">
                        Изменить
                    </button>
                </div>
                <div class="btn">
                    <button type="button" class="btn btn-dark" onclick="window.location.href='/'">
                        Отмена
                    </button>
                </div>
            </div>
        </form>
    @else
        <form action="{{url('/addUser')}}" method="post" class="form-horizontal">
            @csrf
            <div class=form>
                <div class="form-group col-md-5">
                    <label for="name"> Имя </label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
                <div class="form-group col-md-5">
                    <label for="password"> Пароль </label>
                    <input type="text" class="form-control" id="password" name="password">
                </div>
                <div class="form-group col-md-5">
                    <label for="email"> Email </label>
                    <input type="text" class="form-control" id="email" name="email">
                </div>
                <div class="form-group col-md-5">
                    <label for="telegram"> Telegram </label>
                    <input type="text" class="form-control" id="telegram" name="telegram">
                </div>
                <div class="form-group col-md-5">
                    <label for="status"> Статус </label>
                    <select name="status" class="form-control" id="status">
                        <option>ADMIN</option>
                        <option>USER</option>
                        <option>GUEST</option>
                    </select>
                </div>
                <div class="btn">
                    <button type="submit" class="btn btn-primary">
                        Создать
                    </button>
                </div>
                <div class="btn">
                    <button type="button" class="btn btn-dark" onclick="window.location.href='/'">
                        Отмена
                    </button>
                </div>
            </div>
        </form>
    @endif
@endsection
