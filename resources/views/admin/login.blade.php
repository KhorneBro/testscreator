<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Tests Creator</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Bootstrap css and JS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
            integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
            integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
            crossorigin="anonymous"></script>

</head>
<body class="hold-transition login-page">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark " style="margin: 5px">
    <a class="navbar-brand col-md-5" href="{{url('/registration')}}"> Зарегистрироваться</a>
</nav>

<div class="login-box" style="margin: 5px">
    <!-- /.login-logo -->
    <div class="login-box-body" style="margin-top: 2px; padding: 2px">

        <form method="POST" action="{{ url('/login') }}">
            @csrf
            <div class="form-group has-feedback col-md-5 {{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" id="email" class="form-control " placeholder="Email" name="email"
                       value="{{ old('email') }}" required autofocus>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="help-block">
                       <strong>{{ $errors->first('email') . " не верный логин " }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group has-feedback col-md-5 {{ $errors->has('password') ? ' has-error' : '' }}">
                <input type="password" id="password" class="form-control" name="password" required
                       placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))
                    <span class="help-block">
                       <strong>{{ $errors->first('password') . " не верный пароль " }}</strong>
                    </span>
                @endif
            </div>


            <div class="col-md-5">
                <div class="col-md-5">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                        </label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="btn btn-group">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
        <form method="GET" action="{{url('/resetEmailForm')}}" class="col-md-5">
            @csrf
            <div class="btn btn-group">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Foggot Password?</button>
            </div>
        </form>
    </div>
    <!-- /.login-box-body -->
</div>


<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>

</body>
</html>


