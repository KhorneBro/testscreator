@extends('admin.layouts.main')

@section('css')
@endsection

@section('js')
    {{--    <script src="/resources/js/user/user.js"></script>--}}
@endsection

@section('content')
    @foreach($users as $person)
        <div class="card container" style="width: 18rem; border: none">
            <ul class="list-group ">
                <li class="list-group-item list-group-item-info">{{$person->name}}</li>
                <li class="list-group-item list-group-item-light">{{$person->email}}</li>
                <li class="list-group-item list-group-item-light">{{$person->telegram ? $person->telegram : 'Отсутствует' }}</li>
                <li class="list-group-item list-group-item-danger">{{$person->status}}</li>
            </ul>
        </div>

        <div class="container-md col-5 align-items-center">
            <div class="list-group">
                <li class="list-group-item list-group-item-info">{{$person->name}}</li>
                <li class="list-group-item list-group-item-light">{{$person->email}}</li>
                <li class="list-group-item list-group-item-light">{{$person->telegram ? $person->telegram : 'Отсутствует' }}</li>
                <li class="list-group-item list-group-item-danger">{{$person->status}}</li>
            </div>
        </div>
    @endforeach
@endsection
