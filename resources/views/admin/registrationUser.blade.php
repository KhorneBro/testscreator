@extends('admin.layouts.main')

@section('css')
@endsection

@section('content')
    <form action="{{url('/registration')}}" method="post" class="form-horizontal">
        @csrf
        <div class=form>
            <div class="form-group col-md-5">
                <label for="name"> Имя </label>
                <input type="text" class="form-control" id="name" name="name">
            </div>
            <div class="form-group col-md-5">
                <label for="password"> Пароль </label>
                <input type="text" class="form-control" id="password" name="password">
            </div>
            <div class="form-group col-md-5">
                <label for="email"> Email </label>
                <input type="text" class="form-control" id="email" name="email">
            </div>
            <div class="form-group col-md-5">
                <label for="telegram"> Telegram </label>
                <input type="text" class="form-control" id="telegram" name="telegram">
            </div>
            <div class="form-group col-md-5" hidden>
                <label for="status"> Статус </label>
                <select name="status" class="form-control" id="status">

                    <option>USER</option>

                </select>
            </div>
            <div class="btn">
                <button type="submit" class="btn btn-primary">
                    Создать
                </button>
            </div>
            <div class="btn">
                <button type="button" class="btn btn-dark" onclick="window.location.href='/'">
                    Отмена
                </button>
            </div>
        </div>
    </form>
@endsection
