<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tests extends Model
{
    public $incrementing = true;
    public $timestamps = true;
    protected $table = 'tests';
    use HasFactory;

    protected $fillable = [
        'question',
        'picture',
        'answer'
    ];

    public function users()
    {
        return $this->belongsTo('App\Models\User');
    }
}
