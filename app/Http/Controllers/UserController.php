<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function administrators()
    {
        return view('admin.administrators',
            [
                'administrators' => User::all()
            ]);
    }

    public function createUser()
    {
        return view('admin.createUser');
    }

    public function addUser(Request $request)
    {
        $validation = Validator::make($request->all(),
            [
                'name' => 'required|string|max:255',
                'email' => 'required|string|max:255|email|unique:users|regex:([\w.-]+@([\w-]+)\.+\w{2,})',
                'password' => 'required|string|max:255|min:6',
                'status' => 'required|string',
                'telegram' => 'string|nullable'
            ],
            [
                'email.email' => ':attribute заполнен не верно',
                'required' => 'поле :attribute не заполнено',
                'min' => 'поле :attribute не должно быть меньше 6 символов'
            ]);

        if ($validation->fails()) {
            $response =
                [
                    'success' => false,
                    'errors' => $validation->getMessageBag()->toArray()
                ];
            return response()->json($response, 422);

        } else {
            $user = [
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('password')),
                'status' => $request->input('status'),
                'telegram' => $request->input('telegram'),
            ];
            User::create($user);
        }
        response()->json(['success' => true], 200);
        return redirect('/administrators');
    }

    public function registrationView()
    {
        return view('admin.registrationUser');
    }

    public function registrateUser(Request $request)
    {
        $validation = Validator::make($request->all(),
            [
                'name' => 'required|string|max:255',
                'email' => 'required|string|max:255|email|unique:users|regex:([\w.-]+@([\w-]+)\.+\w{2,})',
                'password' => 'required|string|max:255|min:6',
                'status' => 'required|string',
                'telegram' => 'string|nullable'
            ],
            [
                'email.email' => ':attribute заполнен не верно',
                'required' => 'поле :attribute не заполнено',
                'min' => 'поле :attribute не должно быть меньше 6 символов'
            ]);

        if ($validation->fails()) {
            $response =
                [
                    'success' => false,
                    'errors' => $validation->getMessageBag()->toArray()
                ];
            return response()->json($response, 422);

        } else {
            $user = [
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('password')),
                'status' => 'USER',
                'telegram' => $request->input('telegram'),
            ];
            User::create($user);
        }
        response()->json(['success' => true], 200);
        return redirect('/login');
    }

    public function updateUser(Request $request, $id)
    {
        $validation = Validator::make($request->all(),
            [
                'name' => 'required|string|max:255',
                'email' => 'required|string|max:255|email|regex:([\w.-]+@([\w-]+)\.+\w{2,})',
                'password' => 'required|string|max:255|min:6',
                'status' => 'required|string',
                'telegram' => 'string|nullable'
            ],
            [
                'email.email' => ':attribute заполнен не верно',
                'required' => 'поле :attribute не заполнено',
                'min' => 'поле :attribute не должно быть меньше 6 символов'
            ]);

        if ($validation->fails()) {
            $response =
                [
                    'success' => false,
                    'errors' => $validation->getMessageBag()->toArray()
                ];
            return response()->json($response, 422);
        } else {
            $user = User::find($id);
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            $user->status = $request->input('status');
            $user->telegram = $request->input('telegram');

            if ($user->save()) {
                response()->json(['success' => true], 200);
                return redirect('/administrators');
            } else {
                return response()->json(['success' => false], 422);
//                return redirect('/');
            }
        }
    }

    public function editUser($id)
    {
        return view('admin.createUser',
            [
                'user' => User::where('id', $id)->first()
            ]);
    }

    public function deleteUser($id)
    {
        if (User::where('id', $id)->delete()) {
            response()->json(['success' => true], 200);
            return redirect('/administrators');
        } else {
            return response()->json(['succcess' => false], 422);

        }
    }

    public function userInfo($id)
    {
//        dd(User::where('id', $id)->get());
        return view('admin.user',
            [
                'users' => User::where('id', $id)->get()
            ]);
    }


}
