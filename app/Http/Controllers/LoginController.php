<?php

namespace App\Http\Controllers;

use Illuminate\Auth\Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    use Authenticatable;

    protected $redirectTo = '/home';

    public function authenticated(Request $request)
    {
        if (Auth::guard('user')->attempt(
            [
                'email' => $request->email,
                'password' => $request->password,
            ], $request->remeber)) {
            return redirect()->intended('/administrators');
        }
        return redirect()->back();
    }

    public function showForm(Request $request)
    {
        $urlPrevious = url()->previous();
        $urlBase = url()->to('/');
        if (($urlPrevious != $urlBase . '/login_for') && (substr($urlPrevious, 0, strlen($urlBase) === $urlBase))) {
            session()->put('url.intended', $urlPrevious);
        }
        return view('admin.login');
    }

    public function logout()
    {
        Auth::guard('user')->logout();
        return redirect('/login');
    }


    public function guard()
    {
        return Auth::guard('user');
    }
}
