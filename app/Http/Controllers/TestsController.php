<?php

namespace App\Http\Controllers;

use App\Models\Tests;
use Illuminate\Http\Request;

class TestsController extends Controller
{
    public function tests()
    {
        return view('tests.tests',
            [
                'tests' => Tests::all()
            ]);
    }

    /**
     */
    public function createTests(Request $request)
    {
        $this->validate($request,
            [
                'question' => 'required|string',
                'picture' => 'string',
                'answer' => 'required|string'
            ]);

        $request->user()->tests()->create(
            [
                'question' => $request->question,
                'picture' => $request->file()->store('picture'),
                'answer' => $request->answer
            ]
        );
    }

    public function updateTests(Request $request, $id)
    {
        $this->validate($request,
            [
                'question' => 'required|string',
                'picture' => 'string',
                'answer' => 'required|string'
            ]);

        $test = Tests::find($id);
        $test->question = $request->input('question');
        $test->picture = $request->file()->store('picture');
        $test->answer = $request->input('answer');
    }

    public function deleteTests($id)
    {
        if (Tests::where('id', $id)->delete()) {
            return response()->json(['success' => true], 200);
        } else {
            return response()->json(['success' => false], 422);
        }
    }

    public function edtTest($id)
    {
        return Tests::where('id', $id)->first();
    }
}
