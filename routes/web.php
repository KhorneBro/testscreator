<?php

use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/login', [LoginController::class, 'authenticated']);
Route::get('/login', [LoginController::class, 'showForm'])->name('login');
Route::post('/registration', [UserController::class, 'registrateUser']);
Route::get('/registration', [UserController::class, 'registrationView']);

Route::middleware('auth:user')->group(function () {
    Route::get('/logout', [LoginController::class, 'logout']);

    Route::get('/administrators', [UserController::class, 'administrators']);
    Route::post('/addUser', [UserController::class, 'addUser']);
    Route::get('/createUser', [UserController::class, 'createUser']);
    Route::get('/editUser/{id}', [UserController::class, 'editUser'])->where('id', '[0-9]+');
    Route::post('/updateUser/{id}', [UserController::class, 'updateUser'])->where('id', '[0-9]+');
    Route::delete('/deleteUser/{id}', [UserController::class, 'deleteUser'])->where('id', '[0-9]+');
    Route::get('userInfo/{id}', [UserController::class, 'userInfo'])->where('id', '[0-9]+');
});
